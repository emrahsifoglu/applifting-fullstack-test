# Applifting FullStack test - STFUANDCLICK

### Technologies used

* [Linux Mint 18](https://www.linuxmint.com/) - OS
* [PhpStorm](https://www.jetbrains.com/phpstorm/) - The IDE
* [Chromium](https://www.chromium.org/Home) - Browser
* [FireFox](https://www.mozilla.org/) - Browser
* [ReactJS](https://facebook.github.io/react/tutorial/tutorial.html)
* [Bootstrap](http://getbootstrap.com/)

### Prerequisites

* NodeJS

### Installing

To install dependencies you should run the command below.

```
yarn install
```

## Running

You can simply run `npm start` then you should access [http://localhost:8080](http://localhost:8080).

At this point error messages may be appearing on console because site is establishing connection to server.

Therefore you also need to run `npm run server`.

## How site works?

When client is connected, server will send a session id, which will be used to track user's clicking.

If server stops, client will try to re-connect. Server will create a new session but since client has it, this one won't be used.

However if page is refreshed then tracking data will also be gone.

## Pros
It works!

## Cons
- There are no tests to run.
- There can be more validation.
- No loading information is shown.
- Design may need to be improved

## Authors

* **Emrah Sifo?lu** - *Initial work* - [emrahsifoglu](https://github.com/emrahsifoglu)

## License

This project is a task thus source is kept in a private repo.

Resources
========

- https://www.typescriptlang.org/docs/handbook/react-&-webpack.html
- https://github.com/webpack/webpack/issues/981
- https://github.com/iceekey/tslint-react-recommended
- http://blog.devcanvas.org/how-to-create-a-new-typescript-project-using-webpack-3/
- https://www.jetbrains.com/help/webstorm/tslint.html
- https://react-bootstrap.github.io/components.html
- http://serverless-stack.com/chapters/create-containers.html
- https://stackoverflow.com/questions/12251234/how-to-design-a-full-width-bar-with-html-and-css
- https://github.com/webpack-contrib/extract-text-webpack-plugin/issues/263
- https://stackoverflow.com/questions/18933985/this-setstate-isnt-merging-states-as-i-would-expect
- https://stackoverflow.com/questions/7352164/update-all-clients-using-socket-io
- http://beatscodeandlife.ghost.io/react-socket-io-part-i-real-time-chat-application/
- https://github.com/eddiezane/nyc-react-meetup-message-board/blob/master/src/index.js
- https://codereviewvideos.com/blog/how-i-fixed-uncaught-referenceerror-dispatch-is-not-defined/
- https://codepen.io/stowball/post/a-dummy-s-guide-to-redux-and-thunk-in-react
- https://stackoverflow.com/questions/10058226/send-response-to-all-clients-except-sender-socket-io
- https://stackoverflow.com/questions/31553155/sort-by-object-key-in-descending-order-on-javascript-underscore
- https://egghead.io/lessons/javascript-redux-creating-data-on-the-server
- http://lorenstewart.me/2016/11/27/a-practical-guide-to-redux/
- https://scotch.io/tutorials/bookshop-with-react-redux-ii-async-requests-with-thunks
- http://jsfiddle.net/quyB6/8/show/
- https://jsfiddle.net/DTcHh/23855/
- https://stackoverflow.com/questions/18672452/left-align-and-right-align-within-div-in-bootstrap
- https://css-tricks.com/left-align-and-right-align-text-on-the-same-line/
