import express from 'express';
import { Server } from 'http';
import socket from 'socket.io';
import randtoken from 'rand-token';
import uniqid from 'uniqid';

let port = 3000;
let app = express();
let server = Server(app);
let io = socket(server);
let connections = [];

io.on('connection', function(socket) {
    socket.once('disconnect', function () {
        connections.splice(connections.indexOf(socket), 1);
        socket.disconnect();
        console.log('Disconnected: %s socket(s) remaining.', connections.length);
    });
    connections.push(socket);

    let session = uniqid(randtoken.uid(4) + '-');
    socket.emit('connected', session);
    console.log('Connected: %s socket(s) connected.', connections.length);
});

server.listen(port, function(err) {
    if (err) {
        console.log('error', err);
    } else {

    }
});
