import * as actions from '../constants/actionTypes';
import Team from '../models/Team';
import TeamList from '../models/TeamList';
import axios from 'axios';

const api = 'http://localhost:4200/api/v1';

export function increaseClicksSuccess(name: string, clicks: number, ownClicks: number) {
    return {
        type: actions.INCREASE_CLICKS_SUCCESS, name, clicks, ownClicks
    };
}

export function loadLeadersSuccess(leaders: Team[] = []) {
    return {
        type: actions.LOAD_LEADERS_SUCCESS, leaders
    };
}

export const loadLeaders = () => {
    return (dispatch: any) => {
        return axios.get(api + '/leaderboard')
            .then(function (response) {
                dispatch(loadLeadersSuccess(new TeamList(response.data).toArray()));
            })
            .catch(function (error) {
                throw new Error(error);
            });
    };
};

export const increaseClicks = (name: string, session: string) => {
    return (dispatch: any) => {
        return axios.post(api + '/klik', {
            team: name,
            session: session
        }).then(function (response) {
            let clicks = response.data.team_clicks;
            let ownClicks = response.data.your_clicks;
            dispatch(increaseClicksSuccess(name, clicks, ownClicks));
        }).catch(function (error) {
            throw new Error(error);
        });
    };
};
