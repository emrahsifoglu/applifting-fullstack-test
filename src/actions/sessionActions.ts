import * as actions from '../constants/actionTypes';

export function connectSuccess(session: string = '') {
    return {
        type: actions.CONNECT_SUCCESS, session
    };
}
