import * as types from '../constants/actionTypes';

export const sessionReducer = (state: string = '', action: any) => {
    switch (action.type) {
        case types.CONNECT_SUCCESS:
            return action.session;
        default:
            return state;
    }
};

export default sessionReducer;