import * as types from '../constants/actionTypes';

export const ownClicksReducer = (state: number = 0, action: any) => {
    switch (action.type) {
        case types.INCREASE_CLICKS_SUCCESS:
            return action.ownClicks;
        default:
            return state;
    }
};

export default ownClicksReducer;