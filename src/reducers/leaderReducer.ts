import * as types from '../constants/actionTypes';
import Team from '../models/Team';

export const leaderReducer = (state: Team[] = [], action: any) => {
    switch (action.type) {
        case types.LOAD_LEADERS_SUCCESS:
            return action.leaders;
        case types.INCREASE_CLICKS_SUCCESS:
            return state.map(t => updateTeam(t, action));
        default:
            return state;
    }
};

const updateTeam = (state: Team, action: any) => {
    if (state.name !== action.name) {
        return state;
    }

    state.setClicks(action.clicks);

    return state;
};

export default leaderReducer;
