import { combineReducers } from 'redux';
import leaders from './leaderReducer';
import session from './sessionReducer';
import ownClicks from './ownClicksReducer';

export const rootReducer = combineReducers({
    session,
    leaders,
    ownClicks
});

export default rootReducer;
