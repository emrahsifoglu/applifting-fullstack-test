import * as React from 'react';
import { connect } from 'react-redux';
import { TeamList } from '../components/team/TeamList';
import Team from '../models/Team';

interface ILeaderBoardProps {
    teams: Team[];
    limit: number;
    name: string;
}

export class LeaderBoard extends React.Component<ILeaderBoardProps, any> {
    render() {

        let { teams, limit, name } = this.props;

        return (
            <TeamList teams={teams} limit={limit} name={name} />
        );
    }
}

const mapStateToProps = (state: any, ownProps: any) => {
    return {
        teams: state.leaders
    };
};

export default connect(mapStateToProps, null)(LeaderBoard);
