import * as React from 'react';
import { connect } from 'react-redux';
import { increaseClicks } from '../actions/teamActions';
import Team from '../models/Team';
import TeamForm from '../components/team/TeamForm';
import TeamClicks from '../components/team/TeamClicks';

interface ITeamBoardProps {
    increaseClicks: Function;
    session: string;
    ownClicks: number;
    name?: string;
    inputVisible?: boolean;
    teams: Team[];
}

interface ITeamBoardSate {
    name: string;
}

class TeamBoard extends React.Component<ITeamBoardProps, ITeamBoardSate> {

    constructor(props: any, context: any) {
        super(props, context);

        this.state = {
            name: (this.props.name) ? this.props.name : ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.increaseClicks = this.increaseClicks.bind(this);
    }

    handleChange(event: any) {
        this.setState({ name: event.target.value });
    }

    increaseClicks(event: any) {
        event.preventDefault();

        if (!this.state.name) {
            return;
        }

        let { increaseClicks, session } = this.props;
        increaseClicks(this.state.name, session);
    }

    render() {
        let { teams, ownClicks } = this.props;
        let name = this.state.name;
        let clicks = 0;

        if (name) {
            teams.forEach( function(team: Team) {
                if (team.name === name) {
                    clicks = team.clicks;
                }
            });
        }

        return (
            <div>
                <TeamForm
                name={this.state.name}
                inputVisible={this.props.inputVisible}
                onChange={this.handleChange}
                onSave={this.increaseClicks} />
                {!this.props.inputVisible &&
                    <TeamClicks clicks={clicks} ownClicks={ownClicks}/>
                }
            </div>
        );
    }
}

const mapStateToProps = (state: any, ownProps: any) => {
    return {
        session: state.session,
        ownClicks: state.ownClicks,
        teams: state.leaders
    };
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        increaseClicks: (name: string, session: string) => {
            dispatch(increaseClicks(name, session));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamBoard);
