import * as React from 'react';

export const Footer = () => (
    <footer className='footer'>
        <div className='container'>
            <p className='text-center text-italic text-footer'>If you don't like this page, it's <a target='_blank' href='http://www.emrahsifoglu.com'>my</a> fault. I ain't do CSS.</p>
        </div>
    </footer>
);
