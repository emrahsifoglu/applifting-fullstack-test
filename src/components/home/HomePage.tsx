import * as React from 'react';
import TeamBoard from '../../containers/TeamBoard';
import LeaderBoard from '../../containers/LeaderBoard';

export class HomePage extends React.Component<any, any> {
    render() {
        return (
            <div className='page'>
                <div className='page-header home-page-header text-italic'>
                    "It's really simple, you just need to click as much as you can."
                </div>
                <div className='content'>
                    <TeamBoard inputVisible={true}  />
                    <LeaderBoard limit={10} />
                </div>
            </div>
        );
    }
}

export default HomePage;
