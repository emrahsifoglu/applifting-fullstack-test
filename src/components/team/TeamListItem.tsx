import * as React from 'react';
import { Link } from 'react-router-dom';
import { numberFormat } from '../../functions';

export class TeamListItem extends React.Component<any, any> {
    render() {
        let { order, index, name, clicks, selected } = this.props;
        let classNames  = index % 2 === 0 ? 'first' : 'second';

        return (
            <div className={('row team-list-item ' + (!selected ? classNames : 'selected'))}>
                <div className={('col-xs-1 pull-left' + (selected ? ' push-left' : ''))}>{order}</div>
                <div className='col-xs-10'>
                    {!selected ? <Link to={('/') + name}>{name}</Link> : name}
                    <div className={'pull-right text-right push-right'} dangerouslySetInnerHTML={{__html: numberFormat(clicks) }} />
                </div>
            </div>
        );
    }
}
