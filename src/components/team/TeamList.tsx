import * as React from 'react';
import Team from '../../models/Team';
import { TeamListItem } from './TeamListItem';

interface ITeamListProp {
    teams: Team[];
    limit: number;
    name?: string;
}

export class TeamList extends React.Component<ITeamListProp, any> {
    render() {
        let { teams, limit, name } = this.props;
        let end = limit;
        let start = 0;

        if (name) {
            teams.forEach( function(team: Team, index: number) {
                if (team.name === name && index >= end) {
                    start = (index - end) + 1;
                    end += start;
                    return;
                }
            });
        }

        teams = teams.sort((a: any , b: any) => { return b.clicks - a.clicks; } ).slice(start, end);

        let teamList = teams.map((team: Team, index: number) => {
            team.setOrder(start + index + 1);
            return (<TeamListItem
                key={index}
                index={index}
                selected={team.name === name}
                {...team}
            />);
        });

        return (
            <div className='team-list'>
                <div className='row'>
                    <div className='col-xs-1 pull-left'>&nbsp;</div>
                    <div className='col-xs-8 team-list-title'>TEAM</div>
                    <div className='col-xs-3 team-list-title pull-right text-right'>CLICKS</div>
                </div>
                {teamList}
                <div className='bottom text-italic text-center'>
                    Want to be top? STFU and click!
                </div>
            </div>
        );
    }
}
