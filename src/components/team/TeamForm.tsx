import * as React from 'react';
import TextInput from '../common/TextInput';

export default class TeamForm extends React.Component<any, any> {
    render() {
        let { name, onSave, onChange, inputVisible } = this.props;

        return (
            <form onSubmit={onSave} name='team-form' id='team-form'>
                <div className='row'>
                    { inputVisible &&
                    <div className='col-sm-7'>
                        <div className='form-group'>
                            <label className='text-italic' id='label-team-name' htmlFor={name}>Enter your team name</label>
                            <TextInput placeholder='Your mom' name={name} onChange={onChange} value={name} />
                        </div>
                    </div>
                    }
                    <div className={(this.props.inputVisible ? 'col-xs-5' : 'col-xs-12')}>
                        <button type='submit'
                                className={'btn btn-info ' + (this.props.inputVisible ? 'push-right' : null)}
                                id='click'>CLICK!</button>
                    </div>
                </div>
            </form>
        );
    }
}
