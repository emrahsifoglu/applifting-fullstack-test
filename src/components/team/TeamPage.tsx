import * as React from 'react';
import LeaderBoard from '../../containers/LeaderBoard';
import TeamBoard from '../../containers/TeamBoard';

export class TeamPage extends React.Component<any, any> {
    render() {

        let name = this.props.match.params.name;

        return (
            <div className='page'>
                <div className='page-header team-page-header'>
                    Clicking for team <span><strong>{name}</strong></span>
                </div>
                <div className='content'>
                    <TeamBoard name={name} inputVisible={false} />
                    <LeaderBoard name={name} limit={10} />
                </div>
            </div>

        );
    }
}

export default TeamPage;
