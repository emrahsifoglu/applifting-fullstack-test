import * as React from 'react';
import { numberFormat } from '../../functions';

interface ITeamClicksProp {
    clicks: number;
    ownClicks: number;
}

export default class TeamClicks extends React.Component<ITeamClicksProp, any> {
    render() {
        let { clicks, ownClicks } = this.props;

        return (
            <div id='team-clicks'>
                <div className='block-click'>
                    <div className='text-italic'>Your Clicks</div>
                    <div className='text-clicks' dangerouslySetInnerHTML={{__html: numberFormat(ownClicks) }}/>
                </div>
                <div className='block-middle' />
                <div className='block-click'>
                    <div className='text-italic'>Team Clicks</div>
                    <div className='text-clicks' dangerouslySetInnerHTML={{__html: numberFormat(clicks) }}/>
                </div>
            </div>
        );
    }
}
