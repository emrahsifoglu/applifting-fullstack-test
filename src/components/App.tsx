import * as React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { configureStore } from '../store/configureStore';
import * as SocketIOClient from 'socket.io-client';
import { Header } from './common/Header';
import { Footer } from './common/Footer';
import { HomePage } from './home/HomePage';
import { TeamPage } from './team/TeamPage';
import { loadLeaders } from '../actions/teamActions';
import { connectSuccess } from '../actions/sessionActions';
import 'font-awesome/css/font-awesome.css';
import 'bootstrap-css-only';
import '../styles/app.css';

let store = configureStore();

const Main = () => (
    <Switch>
        <Route exact path='/' component={HomePage}/>
        <Route path='/:name' component={TeamPage}/>
    </Switch>
);

const Body = () => (
    <div className='body'>
        <div className='container'>
            <Main/>
        </div>
    </div>
);

export default class App extends React.Component<any, any> {
    constructor() {
        super();
        this.state = {
            socket: {},
            connection: {
                status: 'stateless',
                endpoint: 'http://localhost:3000',
                session: '',
            }
        };
    }

    componentWillMount() {
        this.setState({
            socket: SocketIOClient(this.state.connection.endpoint)
        });
    }

    componentDidMount() {
        this.state.socket.on('connect', this.connect.bind(this));
        this.state.socket.on('connected', this.connected.bind(this));
    }

    connect() {
        let connection = Object.assign({}, this.state.connection);
        connection.status = 'connected';
        this.setState({ connection: connection });
    }

    connected(session: string) {
        let connection = Object.assign({}, this.state.connection);
        connection.session = (connection.session !== '') ? connection.session : session;
        this.setState({ connection: connection });
        store.dispatch(connectSuccess(connection.session)); // ?or this.state.session
        store.dispatch(loadLeaders());
    }

    render() {
        return (
            <Provider store={store}>
                <Router forceRefresh={true}>
                    <div>
                        <Header />
                        <Body />
                        <Footer/>
                    </div>
                </Router>
            </Provider>
        );
    }
}
