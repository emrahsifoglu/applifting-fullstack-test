export default class Team {

  order: number;
  name: string;
  clicks: number;
  ownClicks: number;

  constructor(order: number, name: string, clicks: number) {
    this.order = order;
    this.name = name;
    this.clicks = clicks;
  }

  setClicks = (clicks: number) => {
    this.clicks = clicks;
  }

  setOrder = (order: number) => {
    this.order = order;
  }

}
