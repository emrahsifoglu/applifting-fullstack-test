import Team from './Team';

let list: any = [];

export default class TeamList {

    constructor(teams: any) {
        list = teams.map((team: any) => {
            return new Team(team.order, team.team, team.clicks);
        });
    }

    toArray() {
        return list;
    }

}
