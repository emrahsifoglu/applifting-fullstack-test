export const LOAD_LEADERS = 'LOAD_LEADERS';
export const LOAD_LEADERS_SUCCESS = 'LOAD_LEADERS_SUCCESS';
export const INCREASE_CLICKS = 'INCREASE_CLICKS';
export const INCREASE_CLICKS_SUCCESS = 'INCREASE_CLICKS_SUCCESS';
export const CONNECT_SUCCESS = 'CONNECT_SUCCESS';
