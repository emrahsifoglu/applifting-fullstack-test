const webpack = require('webpack');
const path = require("path");
const WebpackNotifierPlugin = require('webpack-notifier');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const GLOBALS = {
    'process.env.NODE_ENV': JSON.stringify('production'),
    __DEV__: false
};

module.exports = {
    devtool: false,
    entry: [
        './src/index.tsx'
    ],
    output: {
        path: path.resolve(__dirname, 'web'),
        publicPath: '/',
        filename: 'js/bundle.js'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', 'json', '.jsx']
    },
    module: {
        loaders: [{
            test: /\.(ts|tsx)$/,
            enforce: 'pre',
            loader: 'tslint-loader',
            options: {
                failOnHint: true,
                configuration: require('./tslint.json')
            }
        },{
            test: /\.(ts|tsx)$/,
            loader: ['babel-loader', 'ts-loader']
        },{
            test: /(\.css|\.scss)$/,
            loader: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader"
            })
        },{
            test: /\.(png|jpg|jpeg|gif)$/,
            loader: 'url?limit=10000&name=images/[name].[ext]'
        },{
            test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
            loader: 'file-loader',
            options: {
                name: './fonts/[name].[ext]',
            }
        }]
    },
    plugins: [
        new WebpackNotifierPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new ExtractTextPlugin('css/styles.css', {
            publicPath: '/',
            allChunks: true
        }),
        new webpack.DefinePlugin(GLOBALS),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new UglifyJSPlugin({
            sourceMap: true,
            compress: {
                screw_ie8: true, // React doesn't support IE8
                warnings: false
            },
            mangle: {
                screw_ie8: true
            },
            output: {
                comments: false,
                screw_ie8: true
            }
        })
    ],
};
