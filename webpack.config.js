const webpack = require('webpack');
const path = require("path");
const WebpackNotifierPlugin = require('webpack-notifier');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	devtool: 'cheap-module-source-map',
	entry: [
        'webpack-dev-server/client?http://localhost:8080',
		'react-hot-loader/patch',
        './src/index.tsx'
	],
    output: {
        path: path.resolve(__dirname, 'web'),
        publicPath: '/',
        filename: 'js/bundle.js'
    },
	resolve: {
		extensions: ['.ts', '.tsx', '.js', 'json', '.jsx']
	},
	module: {
		loaders: [{
            test: /\.(ts|tsx)$/,
            enforce: 'pre',
            loader: 'tslint-loader',
            options: {
                failOnHint: true,
                configuration: require('./tslint.json')
            }
        },{
			test: /\.(ts|tsx)$/,
			loader: ['react-hot-loader/webpack', 'ts-loader']
		},{
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader"
            })
        },{
            test: /\.(png|jpg|jpeg|gif)$/,
            loader: 'url?limit=10000&name=images/[name].[ext]'
        },{
            test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
            loader: 'file-loader',
            options: {
                name: './fonts/[name].[ext]',
            }
		}]
	},
	plugins: [
		new WebpackNotifierPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new ExtractTextPlugin('css/styles.css', {
            publicPath: '/',
            allChunks: true
        })
	],
	devServer: {
        historyApiFallback: true,
        contentBase: './web',
        hot: true,
        hotOnly: true
	}
};
